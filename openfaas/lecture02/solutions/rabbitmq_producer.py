#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pika, os, logging, sys

logging.basicConfig()

url = os.environ.get('AMQP_URL', 'amqp://guest:guest@localhost:5672/%2f')
params = pika.URLParameters(url)
params.socket_timeout = 5
connection = pika.BlockingConnection(params)
channel = connection.channel()

routing_key = sys.argv[1] if len(sys.argv) > 2 else 'faas-function'
message = ' '.join(sys.argv[2:]) or 'Hello World!'

logging.info("Routing key: %s - Message: %s", routing_key, message)

channel.basic_publish(
    exchange='OpenFaasEx', routing_key=routing_key, body=message)

connection.close()
