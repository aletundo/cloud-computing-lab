#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import requests
import os
import time
import logging
from flask import Flask, request, abort

app = Flask(__name__)
BACKEND_URL = os.environ.get('BACKEND_URL')

if __name__ != '__main__':
    gunicorn_logger = logging.getLogger('gunicorn.error')
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)

@app.route('/get-backend', methods=['GET'])
def webhook():
  try:
    response = requests.get(BACKEND_URL)
    response.raise_for_status()
    logging.info('Get url %s - Response status: %d', BACKEND_URL, response.status_code)
    return response.text
  except requests.exceptions.RequestException as e:
    logging.error('An error occurred: %s', e)

if __name__ == '__main__':
  app.run(host='0.0.0.0', port=5000, debug=False)
