FROM python:3-alpine

WORKDIR /usr/src/app/

ADD requirements.txt ./
RUN pip install -r requirements.txt

ADD ./entrypoint.sh ./
RUN chmod +x entrypoint.sh

EXPOSE 5000

ENTRYPOINT ["./entrypoint.sh"]
CMD ["/usr/local/bin/gunicorn", "-w2", "-b:5000", "client:app"]

ADD ./client.py ./
